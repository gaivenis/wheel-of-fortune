import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:wheel_of_fortune/models/Triangle.dart';
import 'package:wheel_of_fortune/services/wheel_painter.dart';
import 'dart:math' as math;

class Wheel extends StatefulWidget {
  @override
  WheelState createState() => WheelState();
}

class WheelState extends State<Wheel> with SingleTickerProviderStateMixin {
  double rotate = 0.0;
  Animation<double> animation;
  AnimationController controller;
  Offset currentOffset;
  Duration oldDuration = Duration(milliseconds: 0);
  Ticker ticker;
  double velocity;
  bool spinning = false;
  bool turnRight;

  @override
  void initState() {
    super.initState();
    this._initAnimation();
  }

  _initAnimation() {
    ticker = Ticker((Duration duration) {
      oldDuration = duration;
      this.velocity = this.velocity / 1.04 - 0.00001;
      if (this.velocity <= 0) {
        this.ticker.stop();
        this.spinning = false;
      }

      if (rotate > 2 * math.pi) {
        rotate = rotate % (2 * math.pi);
      }

      setState(() {
        if (this.turnRight) {
          rotate += this.velocity;
        } else {
          rotate -= this.velocity;
        }
      });
    });
  }

  _onUpdate(Offset deltaOffset) {
    double newPi = this.currentOffset.direction;
    this.currentOffset = this.currentOffset + deltaOffset;
    double rotation = (newPi - this.currentOffset.direction);
    if (rotation > 0) {
      this.turnRight = false;
    } else {
      this.turnRight = true;
    }

    setState(() {
      this.rotate = this.rotate - rotation;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
            color: Colors.white,
            child: AspectRatio(
              key: Key('AspectWindow'),
              aspectRatio: 1,
              child: GestureDetector(
                onPanStart: (DragStartDetails details) {
                  if (!this.spinning) {
                    this.currentOffset = details.localPosition.translate(
                        -MediaQuery.of(context).size.width / 2,
                        -MediaQuery.of(context).size.width / 2);
                  }
                },
                onPanUpdate: (DragUpdateDetails details) {
                  if (!this.spinning) {
                    this._onUpdate(details.delta);
                  }
                },
                onPanEnd: (DragEndDetails details) {
                  if (!this.spinning) {
                    this.velocity =
                        getVelocity(details.velocity.pixelsPerSecond);
                    this.spinning = true;
                    this.ticker.start();
                  }
                },
                child: CustomPaint(
                  painter: WheelPainter([
                    Triangle(1, Colors.yellow),
                    Triangle(2, Colors.green),
                    Triangle(3, Colors.red),
                    Triangle(4, Colors.blue),
                    Triangle(5, Colors.pink),
                    Triangle(6, Colors.purple),
                    Triangle(7, Colors.orange),
                    Triangle(8, Colors.black54),
                    Triangle(9, Colors.cyan),
                    Triangle(10, Colors.cyan),
                    Triangle(11, Colors.cyan),
                    Triangle(12, Colors.cyan),
                    Triangle(13, Colors.cyan),
                  ], rotate),
                ),
              ),
            )),
        TextButton(
            onPressed: () {
              if (!this.spinning) {
                this.spinning = true;
                this.turnRight = true;
                this.velocity = math.Random().nextDouble();
                ticker.start();
              }
            },
            child: Text('Spin')),
        TextButton(
            onPressed: () {
              this.spinning = false;
              ticker.stop();
            },
            child: Text('Stop'))
      ],
    );
  }

  double getVelocity(Offset offset) {
    double dxSqr = offset.dx * offset.dx;
    double dySqr = offset.dy * offset.dy;
    return math.sqrt(dxSqr + dySqr) / 3000;
  }

  @override
  void dispose() {
    this.controller.dispose();
    super.dispose();
  }
}
