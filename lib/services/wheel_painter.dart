import 'dart:math';

import 'package:flutter/material.dart';
import 'package:wheel_of_fortune/models/Triangle.dart';
import 'dart:math' as math;

class WheelPainter extends CustomPainter {
  final List<Triangle> triangles;
  final double _rotate;
  double scale = 0.95;
  double circleLength = 2 * math.pi;
  
  @override
  WheelPainter(this.triangles, this._rotate);

  @override
  void paint(Canvas canvas, Size size) {
    double lineLength = (size.width / 2) * this.scale;
    int trianglesCount = this.triangles.length;
    double triangleLength = circleLength / trianglesCount;

    Paint paint = Paint();
    paint.color = Colors.blueAccent;
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = 1;
    this.rotate(canvas, _rotate, size);
    double startPoint = 0.0;
    for (Triangle triangle in this.triangles) {
      canvas.save();
      this.rotate(canvas, startPoint, size);
      paint.color = triangle.color;
      this.drawTriangle(canvas, paint, size, triangleLength);  
      this.drawText(canvas, triangle, triangleLength, lineLength, size);
      startPoint += triangleLength;
      canvas.restore();
    }
  }
  
  drawTriangle(Canvas canvas, Paint paint, Size size, double triangleLength) {
    paint.style = PaintingStyle.fill;
    Offset offset = Offset(size.width, size.height) * this.scale;
    canvas.drawArc(Rect.fromPoints(Offset(size.width, size.height) - offset, offset), math.pi * 1.5 - triangleLength / 2, triangleLength, true, paint);
    paint.style = PaintingStyle.stroke;
    paint.color = Colors.white;
    paint.strokeWidth = 3;

    canvas.drawArc(Rect.fromPoints(Offset(size.width, size.height) - offset, offset), math.pi * 1.5 - triangleLength / 2, triangleLength, true, paint);
  }

  drawText(Canvas canvas, Triangle triangle, double triangleLength, double lineLength, Size size) {

    final textStyle = TextStyle(
      color: Colors.white,
      fontSize: 14,
      fontWeight: FontWeight.bold,
      shadows: [
        Shadow(
          blurRadius: 1.4,
          color: Colors.black,
          offset: Offset(0.0, 0.0),
        ),
      ]
    );

    final textSpan = TextSpan(
      text: triangle.point.toString(),
      style: textStyle,
    );
    
    final textPainter = TextPainter(
      text: textSpan,
      textAlign: TextAlign.center,
      textDirection: TextDirection.ltr,
    );

    textPainter.layout(
      minWidth: 0,
      maxWidth: 50,
    );
    
    Offset offset = Offset.fromDirection(math.pi * 1.5, lineLength / 1.5).translate(size.width / 2 - textPainter.size.width / 2, size.height / 2 - textPainter.size.height / 2);
    
    textPainter.paint(canvas, offset);
  }

  void rotate(Canvas canvas, double angle, Size size) {
    final double r = sqrt(size.width * size.width + size.height * size.height) / 2;
    final alpha = atan(size.height / size.width);
    final beta = alpha + angle;
    final shiftY = r * sin(beta);
    final shiftX = r * cos(beta);
    final translateX = size.width / 2 - shiftX;
    final translateY = size.height / 2 - shiftY;
    canvas.translate(translateX, translateY);
    canvas.rotate(angle);
  }

  @override
  bool shouldRepaint(WheelPainter old) {
    return old._rotate != _rotate;
  }
}